
import sys
import os
import process_asc_func
import threading

dirname = "./"
all_dirs = next(os.walk('.'))[1]
print(all_dirs)

class processThread (threading.Thread):
    def __init__(self, filename):
        threading.Thread.__init__(self)
        self.filename = filename

    def run(self):
        process_asc_func.process_asc(self.filename)


for direct in all_dirs:
    fls = os.listdir(dirname + "/" + direct)
    for checkfile in fls:
        if not checkfile.endswith(".asc"): continue
        # This is terrible - don't do this
        # This is really dumb
        #print(dirname + direct + "/" + checkfile + "BEGIN")
        processThread(dirname + direct + "/" + checkfile).start()
        #process_asc_func.process_asc(dirname + direct + "/" + checkfile)
        #print(dirname + direct + "/" + checkfile + "END")

print("Complete")