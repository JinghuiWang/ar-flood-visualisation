
import os
import struct
import sys
import re
import struct
from tqdm import tqdm



filename = sys.argv[1]
#filename = "innerwest_2000y_d.asc"

fl = open(filename)

def remove_spaces(str):
    return re.sub(" +", " ", str)

# Use regex to replace excessive spaces
ncols = remove_spaces(fl.readline()).split(' ')[1].strip()
nrows = remove_spaces(fl.readline()).split(' ')[1].strip()
xllcorner = remove_spaces(fl.readline()).split(' ')[1].strip()
yllcorner = remove_spaces(fl.readline()).split(' ')[1].strip()
cellsize = remove_spaces(fl.readline()).split(' ')[1].strip()
nulldata_value = remove_spaces(fl.readline()).split(' ')[1].strip()

print(ncols, nrows)
print(xllcorner, yllcorner)
print(cellsize, nulldata_value)

# Next line of files is data
# We make a 1d array of size ncols * nrows
data_arr_mapped = []
header_arr = []
header_index = 0
last_check = 0


# before we add all these values, lets add our header first
# HEADER
# unsigned int NCOLS
# unsigned int NROWS
# unsigned int xLLCorner
# unsigned int yLLCorner
HEADER_STRUCT = "IIII"

header_arr.append(int(ncols))
header_arr.append(int(nrows))
header_arr.append(int(xllcorner))
header_arr.append(int(yllcorner))

HEADER_APPEND = "II"

header_arr.append(0)
header_arr.append(0)

def convert_value(x):
    if (x == nulldata_value):
        return -9999
    return float(x)

prev_value = None
num_repetition = 0

def add_index():
    global header_arr
    global header_index
    # Raw sum of uchar count.
    header_arr.append(header_index)
    # / 2 as in c# we will want to skip by this * 5 bytes
    header_arr.append(int(len(data_arr_mapped) / 2))

def add_index_if():
    global last_check
    # Check if file size will be greater or equal to x MB (/ 5 is due to how array is packed)
    if len(data_arr_mapped) - last_check >= 1000:
        last_check = len(data_arr_mapped)
        add_index()

pbar = tqdm(total=os.path.getsize(filename))

while True:
    next_line = fl.readline().strip()
    pbar.update(len(next_line))
    if (next_line == ""): break

    datapoints = [convert_value(x) for x in next_line.split(" ")]
    # For each value here, do the repetition check.
    for x in datapoints:
        # Initialiser
        if prev_value == None:
            prev_value = x
            num_repetition = 0
            continue

        # If x = x - 1, num_repetition++
        # If x != x - 1, append num_repetition, x - 1, set num_repetition to 1 and set x - 1 to x
        # If num_repetition > 255, set to 1 and append values
        if prev_value == x:
            if num_repetition >= 255:
                data_arr_mapped.append(num_repetition)
                data_arr_mapped.append(prev_value)
                header_index += num_repetition
                num_repetition = 1
                add_index_if()
            else:
                num_repetition += 1

            continue
        else:
            data_arr_mapped.append(num_repetition)
            data_arr_mapped.append(prev_value)
            header_index += num_repetition
            prev_value = x
            num_repetition = 1
            add_index_if()

# Put last value in
data_arr_mapped.append(num_repetition)
data_arr_mapped.append(prev_value)

fl.close()

pbar.close()


# Shrink data array (change format to two values:)
# 1 byte unsigned num_repeating
# 4 byte repeated_value

#print(len(data_arr_mapped), int(ncols) * int(nrows))

# fl = open(filename + ".bytes", "wb")
# copy_size = ((len(data_arr_mapped) - 4) / 2)
# fl.write(struct.pack("=" + HEADER_STRUCT + ("Bf" * int(copy_size)), *data_arr_mapped))
# fl.close()

fl = open(filename + ".index.bytes", "wb")
copy_count = int((len(header_arr) - 4) / 2)
fl.write(struct.pack("=" + HEADER_STRUCT + (HEADER_APPEND * copy_count), *header_arr))
fl.close()

print(len(data_arr_mapped))

fl = open(filename + ".bytes", "wb")
copy_count = int((len(data_arr_mapped)) / 2)
fl.write(struct.pack("=" + ("Bf" * copy_count), *data_arr_mapped))
fl.close()