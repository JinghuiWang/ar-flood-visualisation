
import { Router } from "express";
import { GetTerrainHeight, InvalidLocation } from "./data/terrain/get_terrain_height";
const router = Router();

router.get("/terrain/:longgrid/:latgrid", (req, res) => {
    
    let tHeight = 0;

    try {
        tHeight = GetTerrainHeight(Number(req.params.longgrid), 
            Number(req.params.latgrid));
    } catch (e) {
        if(e instanceof InvalidLocation) {
            res.status(400).json({error: e.message});
            return;
        }
    }

    let respData = {
        longitude: req.params.longgrid,
        latitude: req.params.latgrid,
        terrain: tHeight
    };

    res.status(200).json(respData);
});

router.get("/", (req, res) => {
    res.status(200);
    res.json({status: "online"});
});


export default router;