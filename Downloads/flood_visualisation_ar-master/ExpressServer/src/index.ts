
/**
 * This express server is not used or is not intended to use for the prototype
 * It is intended to be used in future development on a scale production version of the application.
 * 
 * Local data storage for local regions is the intended usecase for local testing of the application,
 * however there is far too much data to fit on one device for scale deployment.
 * 
 * Due to the format of the EXPO, it would be dangerous to completely rely on web connectivity for our application
 * demonstration.
 */

import express, { Router } from 'express';
import Routes from "./routes"

const app = express();
const port = 3000;

app.use(Routes);

app.listen(port, () => {
    return console.log(`Express server online: ${port}`);
});