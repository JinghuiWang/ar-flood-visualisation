import fs from 'fs'
import path from "path"

class InvalidLocation implements Error {
    constructor(message: string) {
        this.name = "InvalidLocation";
        this.message = message;
    }

    name: string;
    message: string;
    stack?: string;
}

class TerrainHeight {
    constructor() {
        let lookupFile = fs.readFileSync(path.resolve(__dirname, "./source/lookup.json"), {encoding: "utf-8"});
        console.log(JSON.parse(lookupFile));
    }

    getPoint(easting, northing) {
        
    }
}

function GetTerrainHeight(easting: number, northing: number): number {
    if (easting < 1) {
        throw new InvalidLocation(`Invalid Grid Coordinate ${easting}`);
    }
    
    return 0;
}


const terrainHeight = new TerrainHeight();

export {InvalidLocation, GetTerrainHeight, terrainHeight}