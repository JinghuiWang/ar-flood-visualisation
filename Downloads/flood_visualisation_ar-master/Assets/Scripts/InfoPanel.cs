using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour
{

    public Outline Outline;
    public TMPro.TMP_Text HeightText;
    public TMPro.TMP_Text TitleText;

    void Start()
    {
        TitleText.text = "Undefined";
        HeightText.text = "Depth: ???m";
    }

    public void SetTitle(string title)
    {
        TitleText.text = title;
    }

    public void SetHeight(float depth)
    {
        HeightText.text = string.Format("Depth: {0:0.00}m", depth);
    }
}
