using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FloodData;
using System;

public class FloodDataManager : MonoBehaviour
{
    /* Add new flood data sources here */
    List<IFloodData> floodSources = FloodData.FloodData.FloodSources;


    public GameObject floodPrefab;
    public GameObject infoPanelPrefab;
    public LocationData locationData;

    bool isInfoPanelsOn = false;

    public float RotationStepOffset = 20f;


    List<HeightVisualiser> floodVisualisers = new List<HeightVisualiser>();
    List<InfoPanel> infoPanels = new List<InfoPanel>();

    HashSet<string> enabledSources;
    
    // Start is called before the first frame update
    void Start()
    {
        // MAKE SURE WE ARE AT 0, 0, 0 else the heightvisualisers will be misplaced!!
        this.transform.position = Vector3.zero;

        HashSet<string> uniqueNames = new();

        // Create a gameobject for each source then disable.
        foreach(IFloodData floodSource in floodSources)
        {
            GameObject newFlood = Instantiate(floodPrefab, this.transform);
            HeightVisualiser heightVis = newFlood.GetComponent<HeightVisualiser>();

            // This calls a setter that fills in the important values :)
            heightVis.FloodSource = floodSource;
            
            // Disable this object - wait for GPS fix to be stable
            newFlood.SetActive(false);

            floodVisualisers.Add(heightVis);

            GameObject newInfo = Instantiate(infoPanelPrefab, this.transform);
            InfoPanel panel = newInfo.GetComponent<InfoPanel>();
            //panel.TitleText.color = floodSource.LineColor;
            panel.Outline.effectColor = floodSource.LineColor;
            heightVis.infoPanel = panel;

            infoPanels.Add(panel);

            newInfo.SetActive(false);

            if(!uniqueNames.Add(floodSource.Identifier))
            {
                Debug.LogError(("Conflicting name ", floodSource.Identifier, "; IFloodData names must be unique"));
            }

        }

        enabledSources = uniqueNames;
    }

    public void EnableSource(string name)
    {
        enabledSources.Add(name);
    }

    public void DisableSource(string name)
    {
        enabledSources.Remove(name);
    }

    internal void EnableAllSources()
    {
        enabledSources.Clear();
        foreach(IFloodData floodSource in FloodData.FloodData.FloodSources)
        {
            enabledSources.Add(floodSource.Identifier);
        }
    }

    public void DisableAllSources()
    {
        enabledSources.Clear();
    }

    public void SetSources(string[] names)
    {
        enabledSources.Clear();
        foreach(string name in names)
        {
            enabledSources.Add(name);
        }
    }

    internal void SetInfoPanels(bool isOn)
    {
        isInfoPanelsOn = isOn;
    }

    // Update is called once per frame
    void Update()
    {
        // If we are in the editor run anyway for testing :)
        if(locationData.IsAvailable || Application.isEditor)
        {
            double longitude = locationData.Longitude;
            double latitude = locationData.Latitude;

            // Total height offset currently.
            float heightOffsetCurrent = 0;

            floodVisualisers.Sort((a, b) => a.FloodSource.SeaLevelDepth(longitude, latitude).CompareTo(b.FloodSource.SeaLevelDepth(longitude, latitude)));

            float currentRotationOffset = 0f;

            foreach(HeightVisualiser flood in floodVisualisers)
            {
                flood.RotationAngle = currentRotationOffset % 360;
                currentRotationOffset += RotationStepOffset;
                // Does this floodsource even appear in this location.
                bool isLocationAffected = flood.FloodSource.IsLocationAffected(longitude, latitude);

                // Disable to protect occluder issues and to remove unnecessary rendering on camera
                // Give 5cm buffer to prevent occlusion flickering hopefully
                bool isHeightAboveTerrain = (flood.FloodSource.SeaLevelDepth(longitude, latitude) - 0.05f) > locationData.GetTerrainHeight();

                bool shouldBeActive = isLocationAffected && isHeightAboveTerrain && enabledSources.Contains(flood.FloodSource.Identifier);


                if (shouldBeActive != flood.gameObject.activeSelf)
                {
                    flood.gameObject.SetActive(shouldBeActive);
                    if (shouldBeActive == false)
                    {
                        flood.infoPanel.gameObject.SetActive(shouldBeActive);
                    }
                }
                
                // Apply offsets - This theoretically could be out of sync however I don't really care.
                // Chances are low and its not a dangerous issue. AR is janky enough, this is minor.
                // Not worth the overhead of another loop.
                // Note: If it is out of sync, it will only be wrong for 1 frame
                if(flood.gameObject.activeSelf)
                {
                    float depth = flood.FloodSource.SeaLevelDepth(longitude, latitude);
                    flood.height = depth;
                    flood.groundOffset = heightOffsetCurrent;
                    // Add the difference in height.
                    heightOffsetCurrent += depth - heightOffsetCurrent;

                    if (flood.infoPanel.gameObject.activeSelf != isInfoPanelsOn)
                    {
                        flood.infoPanel.gameObject.SetActive(isInfoPanelsOn);
                    }
                }
            }
        }
    }
}
