using Google.Maps;
using Google.Maps.Event;
using Google.Maps.Feature.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleMapAnchor : MonoBehaviour
{

    public MapsService mapService;
    public LocationData locationData;

    public Material googleMapsModelMaterial;

    GameObjectOptions mapOptions;

    // Start is called before the first frame update
    void Start()
    {
        if(!locationData)
        {
            locationData = FindObjectOfType<LocationData>();
        }

        mapService = FindObjectOfType<MapsService>();
        mapService.Events.MapEvents.Loaded.AddListener(OnMapLoaded);

        mapService.Events.ModeledStructureEvents.DidCreate.AddListener(DidCreateModelStructure);

        mapOptions = ExampleDefaults.DefaultGameObjectOptions;

        ModeledStructureStyle.Builder structureStyle = new ModeledStructureStyle.Builder(mapOptions.ModeledStructureStyle);
        structureStyle.Material = googleMapsModelMaterial;

        ExtrudedStructureStyle.Builder structureExtStyle = new ExtrudedStructureStyle.Builder(mapOptions.ExtrudedStructureStyle);
        structureExtStyle.RoofMaterial = googleMapsModelMaterial;
        structureExtStyle.WallMaterial = googleMapsModelMaterial;

        mapOptions.ModeledStructureStyle = structureStyle.Build();
        mapOptions.ExtrudedStructureStyle = structureExtStyle.Build();

        locationData.LocationUpdate += LocationData_LocationUpdate;
        if (!mapService.Projection.IsFloatingOriginSet)
        {
            mapService.InitFloatingOrigin(new Google.Maps.Coord.LatLng(-27, 153));
        }
        if(locationData.IsAvailable)
        {
            UpdateMapLocation(locationData.Latitude, locationData.Longitude);
        }
    }

    private void OnDestroy()
    {
        mapService.Events.MapEvents.Loaded.RemoveListener(OnMapLoaded);
    }

    private void LocationData_LocationUpdate()
    {
        UpdateMapLocation(locationData.Latitude, locationData.Longitude);
    }

    private void UpdateMapLocation(float latitude, float longitude)
    {
        Debug.Log(mapService);
        ConsoleText.Get()?.AddLine("Google maps updating location");
        mapService.MoveFloatingOrigin(new Google.Maps.Coord.LatLng(latitude, longitude));
        

        mapService.LoadMap(ExampleDefaults.DefaultBounds, mapOptions);
    }

    void OnMapLoaded(MapLoadedArgs args)
    {
        ConsoleText.Get()?.AddLine("Maps loaded");
    }

    void DidCreateModelStructure(DidCreateModeledStructureArgs args)
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
