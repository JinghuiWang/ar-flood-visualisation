using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class BrisbaneCityBelowMinor : IFloodData {
        public string Identifier => "Brisbane City - Below Minor";

        public Color Color => new Color(0.52549f, 0.91765f, 0.94902f, 0.2f);

        public string Description => "A flood of this height should not cause any impact to daily lives";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 1.7f;
        }
    }

}