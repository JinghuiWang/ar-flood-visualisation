using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class HistoricalFlood1841 : IFloodData {
        public string Identifier => "1841 Flood - Biggest Flood on record";

        public Color Color => new Color(178f/255f, 70f/255f, 198f/255f, 0.2f);

        public string Description => "The 1841 Flood is the biggest flood event on record";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 8.43f;
        }
    }

}