using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class HistoricalFlood2022 : IFloodData {
        public string Identifier => "2022 Flood";

        public Color Color => new Color(178f/255f, 70f/255f, 198f/255f, 0.2f);

        public string Description => "The Floods that occured in 2022, the most recent flood event";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 3.8f;
        }
    }

}