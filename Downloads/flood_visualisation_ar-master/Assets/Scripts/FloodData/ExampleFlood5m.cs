using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData
{

    public class ExampleFlood5m : IFloodData
    {

        public string Identifier => "Example 5m Height";

        public Color Color => new Color(1f, 0f, 0f, 0.2f);

        public string Description => "This is an example flood, and poses no real threat";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 5f;
        }
    }

}