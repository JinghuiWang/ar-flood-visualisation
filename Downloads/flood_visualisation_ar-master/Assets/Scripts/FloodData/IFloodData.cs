using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData
{
    public interface IFloodData
    {
        public abstract string Identifier { get; }
        public virtual string Name { get => Identifier; }
        public abstract string Description { get; }
        public abstract Color Color { get; }
        public virtual Color LineColor { 
            get {
                Color lnCol = Color;
                lnCol.a = 1.0f;
                return lnCol;
            } 
        }

        /* Is our location affected at all? */
        public abstract bool IsLocationAffected(double longitude, double latitude);

        /* Return a float of how high above *sea level* the datapoint is at.
         * This function must not fail or throw errors, return -1 if it is not an affected location.
         */
        public abstract float SeaLevelDepth(double longitude, double latitude);

        public virtual Vector3 FlowDirection(double longitude, double latitude)
        {
            return Vector3.zero;
        }        
    }
}