using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData
{

    public class ExampleFlood6m : IFloodData
    {
        public string Identifier => "Example 6m Height";

        public Color Color => new Color(0f, 1f, 0f, 0.2f);

        public string Description => "This is a 6m example flood, and poses no threat";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 6f;
        }
    }

}