using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class BrisbaneCityMajor : IFloodData {
        public string Identifier => "Brisbane City - Major";

        public Color Color => new Color(1f, 0f, 0f, 0.2f);

        public string Description => "Major Flooding : In addition to the above, extensive rural areas and/or urban areas are inundated. Many buildings may be affected above the floor level. Properties and towns are likely to be isolated and major rail and traffic routes closed. Evacuation of flood affected areas may be required. Utility services may be impacted.";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return TerrainData.GetTerrainHeightGPS(longitude, latitude) + 10.0f;
        }
    }

}