using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class BrisbaneCityMinor : IFloodData {
        public string Identifier => "Minor - Brisbane City";

        public Color Color => new Color(0f, 1f, 0f, 0.2f);

        public string Description => "Minor Flooding : Causes inconvenience. Low-lying areas next to watercourses are inundated. Minor roads may be closed and low-level bridges submerged. In urban areas inundation may affect some backyards and buildings below the floor level as well as bicycle and pedestrian paths. In rural areas removal of stock and equipment may be required.";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 2.6f;
        }
    }

}