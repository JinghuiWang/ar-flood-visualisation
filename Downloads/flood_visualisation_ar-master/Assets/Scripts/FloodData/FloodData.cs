using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData { 
    public static class FloodData {
        static List<IFloodData> floodSources = new List<IFloodData> {
            //new ExampleFlood5m(),
            //new ExampleFlood6m(),
            // Disabled temporarily due to magic data
            //new OverlandFlow2yr(),
            //new OverlandFlow10yr(),
            //new OverlandFlow100yr(),
            new StLuciaBelowMinor(),
            new StLuciaMinor(),
            new StLuciaModerate(),
            new StLuciaMajor(),
            new BrisbaneCityBelowMinor(),
            new BrisbaneCityMinor(),
            new BrisbaneCityModerate(),
            new BrisbaneCityMajor(),
            new HistoricalFlood1841(),
            new HistoricalFlood1974(),
            new HistoricalFlood2011(),
            new HistoricalFlood2013(),
            new HistoricalFlood2022()
        };

        public static List<IFloodData> FloodSources { get { return floodSources; } }
    }
}
