using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class BrisbaneCityModerate : IFloodData {
        public string Identifier => "Brisbane City - Moderate";

        public Color Color => new Color(1f, 1f, 0f, 0.2f);

        public string Description => "Moderate Flooding : In addition to the above, the area of inundation is more substantial. Main traffic routes may be affected. Some buildings may be affected above the floor level. Evacuation of flood affected areas may be required. In rural areas removal of stock is required.";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 3.5f;
        }
    }

}