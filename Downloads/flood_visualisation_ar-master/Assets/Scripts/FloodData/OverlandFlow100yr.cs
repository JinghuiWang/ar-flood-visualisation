using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class OverlandFlow100yr : IFloodData {
        public string Identifier => "Overland Flow - 1 in 100 year flood event";

        private string name;

        public string Name => name == null ? Identifier : name;

        public Color Color => new Color(158f/255f, 1.0f, 246f/255f, 0.2f);

        public string Description => "";

        public bool IsLocationAffected(double longitude, double latitude) {
            //return true if value is not -9999
            float depth = GetDepth.ReadData(latitude, longitude, 100, false);
            return depth > 0;
        }

        public float SeaLevelDepth(double longitude, double latitude) {
            float depth = GetDepth.ReadData(latitude, longitude, 100, false);
            float terrainHeight = TerrainData.GetTerrainHeightGPS(longitude, latitude); //since this is measured from AHD

            float velocity = GetDepth.ReadData(latitude, longitude, 100, true) / depth;
            this.name = string.Format("{0}, {1:0.00}m/s", Identifier, velocity);

            return depth + terrainHeight;
        }
    }

}