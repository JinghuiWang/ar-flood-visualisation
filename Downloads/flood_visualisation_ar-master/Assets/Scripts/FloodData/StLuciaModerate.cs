using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FloodData {
    public class StLuciaModerate : IFloodData {
        public string Identifier => "St Lucia - Moderate";

        public Color Color => new Color(1.0f, 1.0f, 0.0f, 0.2f);

        public string Description => "Moderate Flooding : In addition to the above, the area of inundation is more substantial. Main traffic routes may be affected. Some buildings may be affected above the floor level. Evacuation of flood affected areas may be required. In rural areas removal of stock is required.";

        public bool IsLocationAffected(double longitude, double latitude)
        {
            return true;
        }

        public float SeaLevelDepth(double longitude, double latitude)
        {
            return 5.5f;
        }
    }

}