using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using Unity.Collections;

public class InvalidLocation : Exception { }

public struct FileLookupEntry
{
    public int index;
    // NOTE! This is given in whole array values
    // This MUST be * 5 to access the given BYTE.
    public int file_pointer;
}

// File header structure, used to cache lookups to save read times.
public struct FileHeader
{
    public int ncols;
    public int nrows;
    public int xllcorner;
    public int yllcorner;

    public List<FileLookupEntry> data;
}

public static class GetDepth
{
    // Risk level dictionary for FloodDepths
    public static Dictionary<int, Dictionary<string, FileHeader>> dataCacheDepth = new();
    // Risk level dictionary for FloodHazard
    public static Dictionary<int, Dictionary<string, FileHeader>> dataCacheHazard = new();

    static Dictionary<string, FileHeader> GetHeaders(int risk_level, string folder, bool velocity)
    {
        Dictionary<int, Dictionary<string, FileHeader>> dataCache = velocity ? dataCacheHazard : dataCacheDepth;
        if (dataCache.ContainsKey(risk_level))
        {
            return dataCache[risk_level];
        }

        Debug.Log("Load Header");
        string folderName = string.Format("{0}/{1}", folder, risk_level);
        TextAsset[] files = Resources.LoadAll<TextAsset>(folderName);

        Dictionary<string, FileHeader> headerData = new();

        foreach(TextAsset file in files)
        {
            // We only want the index files here.
            if (!file.name.EndsWith(".index")) continue;
            string targetFile = file.name.Replace(".index", "");

            FileHeader header = new FileHeader();
            List<FileLookupEntry> pointerEntries = new();

            NativeArray<byte> data = file.GetData<byte>();
            byte[] header_bytes = new byte[4 * 4];
            NativeArray<byte>.Copy(data, header_bytes, 4 * 4);
            //Buffer.BlockCopy(data, 0, header, 0, 4 * 4);
            header.ncols = BitConverter.ToInt32(header_bytes, 0);
            header.nrows = BitConverter.ToInt32(header_bytes, 4);
            header.xllcorner = BitConverter.ToInt32(header_bytes, 8);
            header.yllcorner = BitConverter.ToInt32(header_bytes, 12);

            int remainingLength = data.Length - 4 * 4;
            // Format: int, int
            // Reuse header_bytes
            for(int i = 0; i < remainingLength; i += 4 * 2)
            {
                NativeArray<byte>.Copy(data, i + 4*4, header_bytes, 0, 4 * 2);
                pointerEntries.Add(new FileLookupEntry { 
                    index = BitConverter.ToInt32(header_bytes, 0),
                    file_pointer = BitConverter.ToInt32(header_bytes, 4)
                });
            }

            header.data = pointerEntries;
            headerData.Add(targetFile, header);
        }
        dataCache.Add(risk_level, headerData);
        return headerData;
    }

    public static float ReadData(double lat, double lon, int risk_level, bool velocity)
    {
        string folder;
        if (velocity) {
            folder = "FloodHazard";
        } else {
            folder = "FloodDepths";
        }

        Dictionary<string, FileHeader> headers = GetHeaders(risk_level, folder, velocity);

        // Now we get to basically copy what is below except we don't load the files
        List<double> gridPoint = GeoToGrid.GeoToGridFunc(lat, lon);
        int easting = (int)Math.Round(gridPoint[0]);
        int northing = (int)Math.Round(gridPoint[1]);

        foreach(KeyValuePair<string, FileHeader> file in headers)
        {
            FileHeader head = file.Value;

            if (head.xllcorner < easting && easting < head.xllcorner + head.ncols && 
                head.yllcorner < northing && northing < head.yllcorner + head.nrows)
            {
                // We need this file! Yay!
                // Load the skip list and figure out where in the file we are going
                List<FileLookupEntry> pointerEntries = head.data;

                int x = easting - head.xllcorner;
                int y = northing - head.yllcorner;

                int accessPoint = x + y * head.ncols;

                FileLookupEntry pointer = pointerEntries.FindLast((m) => m.index <= accessPoint);

                // Now we can load the related TextAsset
                string targetAsset = string.Format("{2}/{0}/{1}", risk_level, file.Key, folder);
                TextAsset file_data = Resources.Load<TextAsset>(targetAsset);
                if (file_data == null) throw new FileNotFoundException(string.Format("TextAsset {0} Not Found", targetAsset));

                NativeArray<byte> data = file_data.GetData<byte>();
                int readHead = pointer.file_pointer * 5;
                int currentIndex = pointer.index;
                // From this point we can read the file
                while(readHead < data.Length)
                {
                    // Compare first as we may have skipped through the data
                    if(currentIndex <= accessPoint)
                    {
                        // We have found our value
                        byte[] value_b = new byte[4];
                        NativeArray<byte>.Copy(data, readHead + 1, value_b, 0, 4);
                        float value = BitConverter.ToSingle(value_b, 0);
                        return value;
                    }
                    // Read byte, add to index
                    byte index = data[readHead];
                    currentIndex += index;

                    readHead += 5;
                }
            }

        }

        return -1f;
    }
}