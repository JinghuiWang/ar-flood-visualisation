using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleText : MonoBehaviour
{
    public static ConsoleText Instance;
    public static ConsoleText Get()
    {
        if(Instance == null)
            Instance = FindObjectOfType<ConsoleText>();

        return Instance;
    }

    public TMPro.TMP_Text consoleText;
    public TMPro.TMP_Text frameTimeText;

    Queue<string> consoleLines = new Queue<string>();

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public void AddLine(string message)
    {
        consoleLines.Enqueue(message);
        if(consoleLines.Count > 64)
        {
            consoleLines.Dequeue();
        }

        consoleText.text = string.Join('\n', consoleLines);
    }

    internal void SetFps(string v)
    {
        frameTimeText.text = v;
    }
}
