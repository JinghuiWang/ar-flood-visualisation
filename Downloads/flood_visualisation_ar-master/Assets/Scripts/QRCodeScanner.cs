using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using ZXing;

public class QRCodeScanner : MonoBehaviour
{
    public TMPro.TMP_Text textAsset;
    ARCameraManager arCameraManager;
    Texture2D qrTex;

    [TextArea(3, 10)]
    public string SimulateCommand;
    public bool SendSimulateCommand;

    string qrCodeText = null;
    bool isLooking = false;
    byte frameAttempts = 0;
    
    public string QRCodeText { get
        {
            string ret = qrCodeText;
            qrCodeText = null;
            return ret;
        } 
    }

    BarcodeReader barcodeReader = new BarcodeReader();
    
    public void EnableSearch()
    {
        textAsset.text = "Enable";
        qrCodeText = null; // Reset here.
        isLooking = true;
    }

    public void DisableSearch()
    {
        isLooking = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        arCameraManager = GetComponent<ARCameraManager>();
        textAsset.text = "Start";
        arCameraManager.frameReceived += ArCameraManager_frameReceived;
        barcodeReader.Options.PossibleFormats = new List<BarcodeFormat>() { BarcodeFormat.QR_CODE };
    }

    private void ArCameraManager_frameReceived(ARCameraFrameEventArgs obj)
    {
        if (qrCodeText != null || !isLooking) return;

        frameAttempts++;

        // 2 times a second
        if (frameAttempts % 15 != 0) return;
        frameAttempts = 0;

        XRCpuImage xRCpuImage;
        if (arCameraManager.TryAcquireLatestCpuImage(out xRCpuImage))
        {
            textAsset.text = "NewImageAquire";
            StartCoroutine(GetQRCode(xRCpuImage));
            xRCpuImage.Dispose();
        }
        
    }

    IEnumerator GetQRCode(XRCpuImage img)
    {
        var result = img.ConvertAsync(new XRCpuImage.ConversionParams() { 
            inputRect = new RectInt(0, 0, img.width, img.height),
            outputDimensions = new Vector2Int(img.width, img.height),
            outputFormat = TextureFormat.ARGB32,
        });

        while(!result.status.IsDone())
        {
            yield return null;
        }

        if(result.status != XRCpuImage.AsyncConversionStatus.Ready)
        {
            Debug.Log("Failed translation");
            yield break;
        }
        textAsset.text = "GetData";
        var rawData = result.GetData<byte>();

        if(qrTex == null)
        {
            qrTex = new Texture2D(result.conversionParams.inputRect.width, result.conversionParams.inputRect.height);
        }

        /*qrTex.LoadRawTextureData(rawData);
        qrTex.Apply();*/

        Result barcodeOut = barcodeReader.Decode(rawData.ToArray(), qrTex.width, qrTex.height, RGBLuminanceSource.BitmapFormat.ARGB32);
        textAsset.text = "OUT";
        if (barcodeOut.Text != null)
        {
            qrCodeText = barcodeOut.Text;
            isLooking = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(SendSimulateCommand)
        {
            SendSimulateCommand = false;
            qrCodeText = SimulateCommand;
        }
    }
}
