using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class CameraHeight : MonoBehaviour
{
    Camera mainCamera;
    public GameObject arSessionObject;
    private ARPlaneManager arPlaneManager;
    private ARRaycastManager arRaycastManager;

    //public GameObject debugTextObject;
    //private TextMeshProUGUI debugTextScript;

    private float m_Height;
    public float Height { get { return m_Height; } }

    private bool isGeographicHeightEnabled = false;
    private float geographicHeight = -1f;
    /* True Height above MSL - This relies on location data! */
    public float GeographicHeight { get { return geographicHeight; } }
    public bool GeographicHeightReady { get { return isGeographicHeightEnabled; } }
    private bool initConfigs = false;

    LocationData locationData;
    ARCameraManager arCamera;

    // Start is called before the first frame update
    void Start()
    {
        arCamera = GetComponent<ARCameraManager>();
        arCamera.frameReceived += ArCamera_frameReceived;


        mainCamera = GetComponent<Camera>();
        arPlaneManager = arSessionObject.GetComponent<ARPlaneManager>();
        arRaycastManager = arSessionObject.GetComponent<ARRaycastManager>();
        //debugTextScript = debugTextObject.GetComponent<TextMeshProUGUI>();
        locationData = GetComponent<LocationData>();
    }

    private void ArCamera_frameReceived(ARCameraFrameEventArgs obj)
    {
        if(!initConfigs)
        {
            initConfigs = true;

            if (arCamera.descriptor.supportsCameraConfigurations)
            {
                using (var configs = arCamera.GetConfigurations(Unity.Collections.Allocator.Temp))
                {
                    foreach (var config in configs)
                    {
                        if (config.framerate == 30 && config.height == 720)
                        {
                            arCamera.subsystem.currentConfiguration = config;
                            //arCamera.subsystem.currentConfiguration;
                            //arCamera.autoFocusRequested = true;
                            ConsoleText.Get()?.AddLine(config.ToString());
                            break;
                            
                        }

                    }
                }
            }
        }
    }

    List<ARRaycastHit> heightRayHits = new();

    // Update is called once per frame
    void Update()
    {
        /**
         * Find lowest plane by raycasting downwards and finding the highest plane.
         */
        
        Ray heightRay = new Ray(this.transform.position, Vector3.down);
        if (arRaycastManager.Raycast(heightRay, heightRayHits, TrackableType.PlaneWithinPolygon))
        {
            float closestDist = 0f;
            TrackableId closestTrackable = TrackableId.invalidId;
            foreach (ARRaycastHit hitPoint in heightRayHits)
            {
                float dist = hitPoint.distance;
                if(dist < closestDist || closestTrackable == TrackableId.invalidId)
                {
                    closestDist = dist;
                    closestTrackable = hitPoint.trackableId;
                }
            }

            // No point continuing to geographic data if we don't have location fix!
            if (closestTrackable == TrackableId.invalidId) return;

            ARPlane plane = arPlaneManager.GetPlane(closestTrackable);

            float cameraHeight = mainCamera.transform.position.y - plane.center.y;
            m_Height = cameraHeight;

        }


        // Calculate true height
        if (locationData.IsAvailable)
        {
            float altitude = locationData.GetTerrainHeight();

            geographicHeight = altitude + Height;

            isGeographicHeightEnabled = true;
        }
    }
}
