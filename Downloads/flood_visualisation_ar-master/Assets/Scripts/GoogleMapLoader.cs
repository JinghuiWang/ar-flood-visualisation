using System.Collections;
using System.Collections.Generic;
using Google.Maps.Coord;
using Google.Maps.Event;
using UnityEngine;
using Google.Maps;
using UnityEngine.UI;
using System;

public class GoogleMapLoader : MonoBehaviour
{
    public GameObject rawImage;

    public InputField x_Input;
    public InputField y_Input;
    public Button btn;
    public Button closeBtn;
    // [Tooltip("LatLng to load (must be set before hitting play).")]
    public LatLng LatLng = new LatLng(-27.2240476, 153.0736333);

    public GameObject mapsPrefab;
    private MapsService mapsService;
    private GameObject curMapsServiceObj;
    /// <summary>
    /// Use <see cref="MapsService"/> to load geometry.
    /// </summary>
    private void Start()
    {
        rawImage.SetActive(false);
        btn.onClick.AddListener(OnBtnClick);
        closeBtn.onClick.AddListener(OnCloseBtnClick);
    }

    private void OnCloseBtnClick()
    {
        if (curMapsServiceObj != null)
        {
            DestroyImmediate(curMapsServiceObj);
            curMapsServiceObj = null;
        }
        rawImage.SetActive(false);
    }

    /// <summary>
    /// Example of OnLoaded event listener.
    /// </summary>
    /// <remarks>
    /// The communication between the game and the MapsSDK is done through APIs and event listeners.
    /// </remarks>
    public void OnLoaded(MapLoadedArgs args)
    {
        // The Map is loaded - you can start/resume gameplay from that point.
        // The new geometry is added under the GameObject that has MapsService as a component.
    }

    public void OnBtnClick()
    {
        if (string.IsNullOrEmpty(x_Input.text) || string.IsNullOrEmpty(y_Input.text))
        {
            return;
        }

        LatLng = new LatLng(double.Parse(x_Input.text), double.Parse(y_Input.text));
        LoadLatLng(LatLng);
    }

    private void LoadLatLng(LatLng latLng)
    {

        if (curMapsServiceObj != null)
        {
            DestroyImmediate(curMapsServiceObj);
            curMapsServiceObj = null;
        }
        rawImage.SetActive(true);
        curMapsServiceObj = Instantiate(mapsPrefab);
        mapsService = curMapsServiceObj.GetComponent<MapsService>();
        // Set real-world location to load.
        mapsService.InitFloatingOrigin(latLng);
        // Register a listener to be notified when the map is loaded.
        mapsService.Events.MapEvents.Loaded.AddListener(OnLoaded);

        mapsService.LoadMap(ExampleDefaults.DefaultBounds, ExampleDefaults.DefaultGameObjectOptions);
    }
}
