using System.Collections;
using System.Collections.Generic;
using static System.Math;
using UnityEngine;

public class GeoToGrid : MonoBehaviour
{
    public float lat;
    public float lon;
    void Start() {
        List<double> GridOutput = GeoToGridFunc((double)lat, (double)lon);
        Debug.Log(GridOutput[0]);
        Debug.Log(GridOutput[1]);
    }

    /**
    * Given the latitude and longitude, returns a Grid Position.
    * Returned as [E, N]
    */
    public static List<double> GeoToGridFunc(double lat, double lon) {
        //convert to radians
        // Debug.Log(lat);
        // Debug.Log(lon);
        double latRad = lat * Mathf.Deg2Rad;
        double lonRad = lon * Mathf.Deg2Rad;
        //GRS80 spheriod values
        int semiMajorAxis = 6378137;
        //double inverseFlattening = 298.257222101;
        // Debug.Log(inverseFlattening);
        //Projection values
        int meridian = 153;
        double scaleFactor = 0.9996f;
        int falseEasting = 500000;
        int falseNorthing = 10000000;

        //calculating easting grid
        //E = (m0*X) + E0
        //.m0 = scale factor
        //.E0 = false easting
        //.X  = A * eta (A = Rectifying Radius, eta = sum of TM ratio)
        //..Rectifying Radius = (semiMajorAxis/(1+n)) * (1 + (n^2)/4 + (n^4)/64 + (n^6)/256 + (n^8)/16384)
        //...n = f / (2-f)
        //....f = 1/inverseFlattening
        //..eta = eta' + sum(alpha_n*cos(2*zeta')*sinh(2*eta'))
        //...alpha_2 = 
        //...alpha_4 = 
        //...alpha_6 = 
        //...alpha_8 = 
        //...alpha_10 = 
        //...alpha_12 = 
        //...alpha_14 = 
        //...alpha_16 = 
        //...eta' = v/semiMajorAxis
        //....v = semiMajorAxis * asinh(sin(omega)/sqrt(t'^2 + (cos(omega))^2))
        //.....omega = meridan - lon (RADIANS!!!!!!!), (longitudinal difference)
        //......t' = t*sqrt(1+sigma^2) - sigma*sqrt(1+t^2)
        //.......t = tan(lat) (RADIANS!!!!)
        //.......sigma = sinh(e*atanh(e*t/sqrt(1+t^2)))
        //........e = sqrt(e^2)
        //.........e^2 = f*(2-f)

        //Ellipsoid constants
        //double f = 3.352810681E-03f;
        double e_2 = 6.694380023E-03;
        double n = 1.679220395E-03;
        double n_2 = 2.819781134E-06;
        //double n_3 = 4.735033988E-09f;
        double n_4 = 7.951165642E-12;
        //double n_5 = 1.335175951E-14f;
        double n_6 = 2.242054687E-17;
        //double n_7 = 3.764903956E-20f;
        double n_8 = 6.322103507E-23;

        // double f = 1/inverseFlattening;
        // double e_2 = f * (2-f);
        // double n = f/(2-f);
        // double n_2 = System.Math.Pow(n, 2);
        // double n_3 = System.Math.Pow(n, 3);
        // double n_4 = System.Math.Pow(n, 4);
        // double n_5 = System.Math.Pow(n, 5);
        // double n_6 = System.Math.Pow(n, 6);
        // double n_7 = System.Math.Pow(n, 7);
        // double n_8 = System.Math.Pow(n, 8);
        //RectifyingRadius
        double rectRadius = ((semiMajorAxis/(1+n)) * (1 + n_2/4 +  n_4/64 +  n_6/256 + n_8/16384));
        // Debug.Log(rectRadius);
        //Conformal latitude
        double t = System.Math.Tan(latRad);
        // Debug.Log(t);
        double e = System.Math.Sqrt(e_2);
        // Debug.Log(e);
        double sigma = sinh(e*atanh(e*t/System.Math.Sqrt(1+System.Math.Pow(t, 2))));
        // Debug.Log(sigma);
        double tP = t*System.Math.Sqrt(1+System.Math.Pow(sigma, 2)) - sigma*System.Math.Sqrt(1+System.Math.Pow(t, 2));
        // Debug.Log(tP);
        //Logitudinal Difference
        double omega = lonRad - meridian*Mathf.Deg2Rad;
        // Debug.Log(omega);
        //Gauss-Schreiber ratios
        double etaP = asinh(System.Math.Sin(omega)/System.Math.Sqrt(tP*tP +System.Math.Pow(System.Math.Cos(omega), 2)));
        // Debug.Log(etaP);
        double zetaP = System.Math.Atan(tP/System.Math.Cos(omega));
        // Debug.Log(zetaP);
        //Gauss-Schreiber coords
        double v = etaP * semiMajorAxis;
        // Debug.Log(v);
        double u = semiMajorAxis*zetaP;
        // Debug.Log(u);
        //coefficients alpha
        double a_2 = 8.377318247286E-04;
        double a_4 = 7.608527848150E-07;
        double a_6 = 1.197645520855E-09;
        double a_8 = 2.429170728037E-12;
        double a_10 = 5.711818510466E-15;
        double a_12 = 1.479997974926E-17;
        double a_14 = 4.107624250384E-20;
        double a_16 = 1.210785086483E-22;
        double eta_6 = (a_6*System.Math.Cos(6*zetaP)*sinh(6*etaP));
        double eta_8 = (a_8*System.Math.Cos(8*zetaP)*sinh(8*etaP));
        double eta_4 = (a_4*System.Math.Cos(4*zetaP)*sinh(4*etaP));
        double eta_2 = (a_2*System.Math.Cos(2*zetaP)*sinh(2*etaP));
        double eta_10 = (a_10*System.Math.Cos(10*zetaP)*sinh(10*etaP));
        double eta_12 = (a_12*System.Math.Cos(12*zetaP)*sinh(12*etaP));
        double eta_14 = (a_14*System.Math.Cos(14*zetaP)*sinh(14*etaP));
        double eta_16 = (a_16*System.Math.Cos(16*zetaP)*sinh(16*etaP));
        double eta = etaP + eta_2 + eta_4 + eta_6 + eta_8 + eta_10 + eta_12 + eta_14 + eta_16;
        // Debug.Log(eta);
        double zeta_2 = (a_2*System.Math.Sin(2*zetaP)*cosh(2*etaP));
        double zeta_4 = (a_4*System.Math.Sin(4*zetaP)*cosh(4*etaP));
        double zeta_6 = (a_6*System.Math.Sin(6*zetaP)*cosh(6*etaP));
        double zeta_8 = (a_8*System.Math.Sin(8*zetaP)*cosh(8*etaP));
        double zeta_10 = (a_10*System.Math.Sin(10*zetaP)*cosh(10*etaP));
        double zeta_12 = (a_12*System.Math.Sin(12*zetaP)*cosh(12*etaP));
        double zeta_14 = (a_14*System.Math.Sin(14*zetaP)*cosh(14*etaP));
        double zeta_16 = (a_16*System.Math.Sin(16*zetaP)*cosh(16*etaP));
        double zeta = zetaP + zeta_2 + zeta_4 + zeta_6 + zeta_8 + zeta_10 + zeta_12 + zeta_14 + zeta_16;
        //TM coords
        double X = rectRadius * eta;
        double Y = rectRadius * zeta;
        // Debug.Log(X);
        double E = scaleFactor*X + falseEasting;
        double N = scaleFactor*Y + falseNorthing;
        // Debug.Log(E);
        return new List<double>(){E, N};
    }

    static double sinh(double x) {
        return (System.Math.Exp(x) - System.Math.Exp(-1*x))/2;
    }

    static double cosh (double x) {
        return (System.Math.Exp(x) + System.Math.Exp(-1*x))/2;
    }

    static double asinh (double x) {
        return System.Math.Log(x + System.Math.Sqrt(x*x + 1), System.Math.Exp(1));
    }

    static double atanh (double  x) {
        return 0.5f*System.Math.Log((1+x)/(1-x), System.Math.Exp(1));
    }
}
