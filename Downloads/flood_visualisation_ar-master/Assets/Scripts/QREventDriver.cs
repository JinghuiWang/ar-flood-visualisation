using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QREventDriver : MonoBehaviour
{

    public TMPro.TMP_Text textAsset;
    public FloodDataManager floodDataManager;

    LocationData locationData;
    QRCodeScanner codeScanner;
    // Start is called before the first frame update
    void Start()
    {
        codeScanner = GetComponent<QRCodeScanner>();
        locationData = GetComponent<LocationData>();
        textAsset.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        string codeScan = codeScanner.QRCodeText;
        if(codeScan != null)
        {
            string[] commands = codeScan.Split("\n");
            foreach(string cmd in commands)
            {
                CommandParser(cmd.Trim());
            }
            
        }
    }

    void CommandParser(string command)
    {
        string[] qrValues = command.Split(";");
        switch (qrValues[0])
        {
            case "SOURCE":
                string[] sourcesList = qrValues[1].Split(",");
                Debug.Log(sourcesList);
                floodDataManager.SetSources(sourcesList);
                if(locationData.ApplyLocationOffset)
                {
                    textAsset.text = "Location Offset is Enabled";
                } else
                {
                    textAsset.text = "Using customised FloodSources";
                }
                break;
            case "SET":
                {
                    float longitude = float.Parse(qrValues[1]);
                    float latitude = float.Parse(qrValues[2]);
                    locationData.SetLongitudeLocation(longitude);
                    locationData.SetLatitudeLocation(latitude);
                    locationData.ApplyLocationOffset = true;
                    textAsset.text = "Location Offset is Enabled";

                    break;
                }
            case "SETOFFSET":
                {
                    float longitude = float.Parse(qrValues[1]);
                    float latitude = float.Parse(qrValues[2]);
                    locationData.LongitudeOffset = longitude;
                    locationData.LatitudeOffset = latitude;
                    locationData.ApplyLocationOffset = true;
                    textAsset.text = "Location Offset is Enabled";

                    break;
                }
            case "RESET":
                {
                    locationData.ApplyLocationOffset = false;
                    locationData.LongitudeOffset = 0f;
                    locationData.LatitudeOffset = 0f;
                    textAsset.text = "";
                    floodDataManager.EnableAllSources();

                    break;
                }
        }
    }
}
