using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LocationData : MonoBehaviour
{
    // Raw access if required for pure location data
    public float m_latitude;
    public float m_longitude;
    float m_altitude;
    float m_horizontalAccuracy;
    double m_timestamp;
    bool m_dataAvailable = false;

    // "Locked" distances, that only update when greater than a certain limit.
    public float boundLatitude;
    public float boundLongitude;

    // Boundary around last 'bound' radius before public Latitude and Longitude is updated
    public float lockedRadius = 0.00005f; // In long/lat this is typically ~5.5m
    
    public float Latitude { 
        get {
            if(ApplyLocationOffset)
            {
                return boundLatitude + LatitudeOffset;
            }
            return boundLatitude;
        } 
    }

    public float Longitude { get
        {
            if(ApplyLocationOffset)
            {
                return boundLongitude + LongitudeOffset;
            }
            return boundLongitude;
        }
    }

    public float Altitude { get => m_altitude; }
    public float Horizontal_Accuracy { get => m_horizontalAccuracy; }
    public double Timestamp { get => m_timestamp; }

    public bool IsAvailable { get => m_dataAvailable; }

    public bool ApplyLocationOffset = false;

    public float LatitudeOffset = 0f;
    public float LongitudeOffset = 0f;

    private float lastLatOffset = 0f;
    private float lastLongOffset = 0f;

    public delegate void OnLocationUpdateHandler();

    public event OnLocationUpdateHandler LocationUpdate;

    public void SetLatitudeLocation(float lat)
    {
        LatitudeOffset = lat - this.Latitude;
    }

    public void SetLongitudeLocation(float lon)
    {
        LongitudeOffset = lon - this.Longitude;
    }


    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.FineLocation))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.FineLocation);
        }
#endif

        Input.location.Start(1f, 1f); 
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.location.status == LocationServiceStatus.Running)
        {
            m_latitude = Input.location.lastData.latitude;
            m_longitude = Input.location.lastData.longitude;
            m_altitude = Input.location.lastData.altitude;
            m_horizontalAccuracy = Input.location.lastData.horizontalAccuracy;
            m_timestamp = Input.location.lastData.timestamp;

            m_dataAvailable = true;
        }

        // Technically we shouldn't update this outside of Status.Running, but its minor and is easier for testing
        if (Mathf.Abs(m_latitude - boundLatitude) > lockedRadius)
        {
            UpdateFloatingPosition();
        }

        if (Mathf.Abs(m_longitude - boundLongitude) > lockedRadius)
        {
            UpdateFloatingPosition();
        }

        if(lastLatOffset != LatitudeOffset || lastLongOffset != LongitudeOffset)
        {
            lastLatOffset = LatitudeOffset;
            lastLongOffset = LongitudeOffset;
            LocationUpdate?.Invoke();
        }
    }

    void UpdateFloatingPosition()
    {
        boundLatitude = m_latitude;
        boundLongitude = m_longitude;

        LocationUpdate?.Invoke();
    }

    // Shortcut method for TerrainData.GetTerrainHeight
    public float GetTerrainHeight()
    {
        if (IsAvailable)
        {
            List<double> gridPosition = GeoToGrid.GeoToGridFunc(m_latitude, m_longitude);
            return TerrainData.GetTerrainHeight(gridPosition[0], gridPosition[1]);
        }
        return -1f;
    }
}
