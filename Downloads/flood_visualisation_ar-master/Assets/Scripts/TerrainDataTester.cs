using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainDataTester : MonoBehaviour
{
    internal class TerrainDataExpected
    {
        public TerrainDataExpected(double longitude, double latitude, float height)
        {
            this.longitude = longitude;
            this.latitude = latitude;
            this.expectedHeight = height;
        }

        public double longitude;
        public double latitude;
        public float expectedHeight;
    }

    // Start is called before the first frame update
    void Start()
    {
        List<TerrainDataExpected> testData = new List<TerrainDataExpected>();

        testData.Add(new TerrainDataExpected(153.0278163, -27.4780436, 3.856f));
        // The Gabba
        testData.Add(new TerrainDataExpected(153.0381216, -27.4859739, 5.668f));
        // UQ Oval (Near bus stop)
        testData.Add(new TerrainDataExpected(153.0189664, -27.5002366, 6.62f));

        foreach (TerrainDataExpected expectedData in testData)
        {
            float heightLocal = TerrainData.GetTerrainHeightGPS(expectedData.longitude, expectedData.latitude);
            float expectedHeight = expectedData.expectedHeight;
            float errorVal = heightLocal - expectedHeight;

            if(Mathf.Abs(errorVal) > 0.01f)
            {
                Debug.Log(string.Format("Error greater than 1cm: {0}m (expected: {1}, got: {2}", errorVal, expectedHeight, heightLocal));
            } else
            {
                Debug.Log(string.Format("Correct Result: {0}m (expected: {1}, got: {2}", errorVal, expectedHeight, heightLocal));
            }
        }
        // "top": 6961000.0, "bottom": 6962000.0, "left": 498000.0, "right": 499000.0
        //TerrainData.LookupList.GetHeightAtArea(498500.5, 6961500.5);
        //TerrainData.LookupList.GetHeightAtArea(498500.75, 6961500.5);
        //TerrainData.LookupList.GetHeightAtArea(498500.75, 6961500.8);q
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
