using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
public class LookupEntryRoot
{
    public List<LookupEntry> data;

    public LookupEntry GetEntryInBound(double easting, double northing)
    {
        foreach(LookupEntry entry in data)
        {
            if (entry.InBounds(easting, northing))
                return entry;
        }

        return null;
    }

    public float GetHeightAtArea(double easting, double northing)
    {
        LookupEntry entry = GetEntryInBound(easting, northing);
        if (entry != null)
            return entry.GetHeightAtArea(easting, northing);

        throw new Exception("This location is not supported");
    }

    public float GetHeightAtPoint(double easting, double northing)
    {
        LookupEntry entry = GetEntryInBound(easting, northing);
        if (entry != null)
            return entry.GetHeightAtPoint(easting, northing);

        Debug.LogWarning(string.Format("{0} {1}, This location is not supported", easting, northing));

        return float.MinValue;
    }
}
[Serializable]
public class LookupEntry
{
    static string imageStorageFolder = "TerrainData/GeneratedData/";
    // These are stored as doubles, as GPS data needs down to 6 decimals of precision
    // normal float is only accurate to 6!!
    public double top;
    public double left;
    public double right;
    public double bottom;
    public string file;

    // This array is 1M * (4B) = 4MB of data
    // Theoretically only up to 9 of these will feasibly be loaded into memory, so we'll just cache them and hope for the best.
    // In future, dropping these once the user is far away, or when they haven't been accessed will save memory, however this is unimportant right now.
    public float[] data;

    public override string ToString()
    {
        return string.Format("top: {0} left: {1} bottom: {2} right: {3} file: {4}", top, left, bottom, right, file);
    }

    /**
     * Is this location within the bounds of this LookupEntry
     */ 
    public bool InBounds(double easting, double northing)
    {
        // If any fail (most likely to be top in the sorting), just say false early.
        if (northing <= this.top) return false;
        if (northing > this.bottom) return false;
        if (easting <= this.left) return false;
        if (easting > this.right) return false;
        return true;
    }

    private struct PixelWeight
    {
        public float value;
        public float weight;
    };

    /**
     * Get the terrain height at a Grid coordinate
     */
    public float GetHeightAtArea(double easting, double northing)
    {
        if (!this.InBounds(easting, northing))
            throw new Exception(string.Format("[{0}, {1}] is out of bounds of [[{2}, {3}], [{4}, {5}]]", northing, easting, this.top, this.left, this.bottom, this.right));

        Debug.Log(string.Format("Easting {0} Northing {1}", easting, northing));

        double closeEasting = easting % 1000;
        // Invert northing.
        double closeNorthing = 1000 - (northing % 1000);

        // Offset by -0.5 so it is zero when the grid is xxxx.5
        double decimalEasting = (closeEasting - Math.Truncate(closeEasting)) - 0.5;
        double decimalNorthing = (closeNorthing - Math.Truncate(closeNorthing)) - 0.5;

        List<PixelWeight> pixels = new List<PixelWeight>();
        // The vector length is the magnitude of the vector.
        // This will be used to normalise our list later, so it adds up to a weight of 1.0
        float vectorLength = 0;
        // Check the 3x3 grid around our center pixel.
        for (int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                // distance^2
                float distance = (float)(Math.Pow(decimalEasting - x, 2) + Math.Pow(decimalNorthing - y, 2));
                float weight = Mathf.Max(1f - distance, 0f);

                if (weight <= 0.001f)
                    continue;
                
                //Debug.Log(string.Format("[{0},{1}] = {2}", x, y, weight));

                // Get relative positions, use original values for this.
                // GetHeightAtPoint targets the specific point, and doesn't run averaging.
                float newPositionHeight = TerrainData.LookupList.GetHeightAtPoint(easting + x, northing - y);
                Debug.Log(string.Format("NextEasting {0} nextNorthing {1}", easting + x, northing + y));


                pixels.Add(new PixelWeight { value = newPositionHeight, weight = weight });

                vectorLength += weight;
            }
        }

        if(vectorLength != 1)
        {
            for(int i = 0; i < pixels.Count; i++)
            {
                PixelWeight pxl = pixels[i];
                pxl.weight = (float)(pxl.weight / vectorLength);
                pixels[i] = pxl;
            }
        }

        float groundHeightAverage = 0f;
        foreach(PixelWeight point in pixels)
        {
            //Debug.Log(string.Format("Normalised: {0}, {1}", point.value, point.weight));
            groundHeightAverage += point.value * point.weight;
        }

        Debug.Log(string.Format("Number of weights: {0}", pixels.Count));
        foreach(PixelWeight point in pixels)
        {
            Debug.Log(string.Format("WeightedValue: {0}, Weight: {1}, Value: {2}", point.value * point.weight, point.weight, point.value));
        }

        return groundHeightAverage;
    }

    public float GetHeightAtPoint(double easting, double northing)
    {
        if (data == null)
            LoadData();

        return data[CoordinateToPixel(easting, northing)];
    }

    /* Loads or reloads the cache. */
    private void LoadData()
    {
        // Load the file as a TextAsset.
        TextAsset dataFile = Resources.Load<TextAsset>(this.GetResourceLocation());
        // Each file is 4MB (1000 x 1000 floating point array)
        data = new float[1000 * 1000];
        Buffer.BlockCopy(dataFile.bytes, 0, data, 0, dataFile.bytes.Length);
    }

    private string GetResourceLocation()
    {
        return imageStorageFolder + this.file.Replace(".bytes", "");
    }

    /**
     * Convert a coordinate value to a pixel value on the reference height map
     */ 
    int CoordinateToPixel(double easting, double northing)
    {
        // Grid coordinates are in meters, modulo the km out of the question and we have pixel coordinates.
        // InBounds guarantees that it is within the 0..999 square of the pixel.
        double closeEasting = easting % 1000;
        // Invert northing.
        double closeNorthing = 1000 - (northing % 1000);

        int eastingPixel = (int)Math.Ceiling(closeEasting);
        int northingPixel = (int)Math.Ceiling(closeNorthing);

        return PixelToIndex(eastingPixel, northingPixel);
    }

    int PixelToIndex(int x, int y)
    {
        return x + y * 1000;
    }
}

public static class TerrainData
{
    static string lookupFile = "TerrainData/GeneratedData/lookup"; // No extension (for some reason, thanks Unity)
    static LookupEntryRoot lookupList;

    private static void InitLookup() 
    {
        //Debug.Log("LookupList is generating!");
        TextAsset lookupFileAsset = Resources.Load<TextAsset>(lookupFile);

        lookupList = JsonUtility.FromJson<LookupEntryRoot>(lookupFileAsset.text);
    }

    public static float GetTerrainHeightGPS(double longitude, double latitude)
    {
        List<double> gridPosition = GeoToGrid.GeoToGridFunc(latitude, longitude);
        return GetTerrainHeight(gridPosition[0], gridPosition[1]);
    }

    public static float GetTerrainHeight(double easting, double northing)
    {
        return LookupList.GetHeightAtPoint(easting, northing);
    }

    public static LookupEntryRoot LookupList { get {
            if (lookupList == null)
                InitLookup();

            return lookupList;
        } 
    }
}
