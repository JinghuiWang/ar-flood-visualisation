using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Map : MonoBehaviour
{
   public Button Close;
   public GameObject cameraPage;
    // Start is called before the first frame update
    void Start()
    {
        Close.onClick.AddListener(CloseEvent);
    }

    void CloseEvent()
    {
        gameObject.SetActive(false);
        cameraPage.SetActive(true);
    }

    
}