using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BG : MonoBehaviour
{
   public Button btn;
   public Button info;
   public Button data;
   public GameObject self;
   //public GameObject target1;
   public GameObject target2;
   public GameObject target3;

   public bool ShouldSwitchScene = true;



    // Start is called before the first frame update
    void Start()
    {
        btn.onClick.AddListener(BtnEvent);
        info.onClick.AddListener(InfoEvent);
        data.onClick.AddListener(DataEvent);
    }

    void BtnEvent()
    {
        //self.SetActive(false);
        //target2.SetActive(false);
        //target3.SetActive(false);
        //target1.SetActive(true);

        //if (ShouldSwitchScene)
        {
            // Switch to the dedicated AR View Scene if we are not testing UI
            SceneManager.LoadScene("ARViewScene", LoadSceneMode.Single);
        }
    }
    void InfoEvent()
    {
        self.SetActive(false);
        //target1.SetActive(false);
        target3.SetActive(false);
        target2.SetActive(true);
    }
    void DataEvent()
    {
        self.SetActive(false);
        //target1.SetActive(false);
        target2.SetActive(false);
        target3.SetActive(true);
    }

 
}
