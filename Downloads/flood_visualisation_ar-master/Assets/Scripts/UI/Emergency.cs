using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Emergency : MonoBehaviour
{
   public Button Close;
   public GameObject close;
   public GameObject main_page;

    public Toggle PhoneNumberToggle;
    public Toggle FloodKonwledgeToggle;

    public ScrollRect PhoneNumberScr;
    public ScrollRect FloodKonwledgeScr;

    private void OnEnable()
    {
        PhoneNumberToggle.isOn = true;
        FloodKonwledgeToggle.isOn = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        Close.onClick.AddListener(CloseEvent);
    }

    void CloseEvent()
    {
        close.SetActive(false);
        main_page.SetActive(true);
    }

    private void OnDisable()
    {
        PhoneNumberToggle.isOn = true;
        FloodKonwledgeToggle.isOn = false;
        PhoneNumberScr.verticalNormalizedPosition = 1;
        FloodKonwledgeScr.verticalNormalizedPosition = 1;
    }
}