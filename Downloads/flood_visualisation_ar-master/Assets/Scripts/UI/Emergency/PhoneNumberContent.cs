using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PhoneNumberContent : MonoBehaviour
{

    public Button AddButton;
    public GameObject AddPanel;
    public Button ConfirmButton;
    public Button CancelButton;
    public TMP_InputField NameInput;
    public TMP_InputField PhoneNumInput;

    public GameObject PhoneNumberItemPrefab;
    public Transform ItemParent;

    private List<GameObject> itemObjList=new List<GameObject>();
    private void OnEnable()
    {
        string str = PlayerPrefs.GetString("PhoneName", "");
        string[] ary= str.Split("|");

        for (int i = 0; i < ary.Length; i++)
        {
            if (string.IsNullOrEmpty(ary[i]))
            {
                continue;
            }

            GameObject item = Instantiate(PhoneNumberItemPrefab, ItemParent);
            item.transform.localScale = Vector3.one;
            itemObjList.Add(item);

            PhoneNumberItem phoneNumberItem = item.GetComponent<PhoneNumberItem>();
            phoneNumberItem.SetInfo(ary[i], PlayerPrefs.GetString($"{ary[i]}"));
        }
    }



    void Start()
    {
        AddButton.onClick.AddListener(OnAddButtonClick);
        ConfirmButton.onClick.AddListener(OnConfirmButtonClick);
        CancelButton.onClick.AddListener(OnCancelButtonClick);
    }

    /// <summary>
    /// open adding contact interface
    /// </summary>
    private void OnAddButtonClick()
    {
        AddPanel.SetActive(true);
        NameInput.text = "";
        PhoneNumInput.text = "";
    }

    /// <summary>
    /// confrim adding contact
    /// </summary>
    private void OnConfirmButtonClick()
    {
        if ((!string.IsNullOrEmpty(NameInput.text)) && (!string.IsNullOrEmpty(PhoneNumInput.text)))
        {
         
            PlayerPrefs.SetString($"{NameInput.text}", PhoneNumInput.text);
            string str = PlayerPrefs.GetString("PhoneName", "");
            str += $"|{NameInput.text}";
            PlayerPrefs.SetString("PhoneName", str);

            GameObject item = Instantiate(PhoneNumberItemPrefab, ItemParent);
            item.transform.localScale = Vector3.one;
            itemObjList.Add(item);

            PhoneNumberItem phoneNumberItem = item.GetComponent<PhoneNumberItem>();
            phoneNumberItem.SetInfo(NameInput.text,PhoneNumInput.text);
            AddPanel.SetActive(false);
        }
    }


    /// <summary>
    /// cancel adding contact
    /// </summary>
    private void OnCancelButtonClick()
    {
        AddPanel.SetActive(false);
        NameInput.text = "";
        PhoneNumInput.text = "";
    }

    private void OnDisable()
    {
        for (int i = 0; i < itemObjList.Count; i++)
        {
            Destroy(itemObjList[i]);
        }
        itemObjList.Clear();

        OnCancelButtonClick();
    }
}
