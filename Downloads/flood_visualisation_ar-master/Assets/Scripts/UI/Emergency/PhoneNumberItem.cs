using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class PhoneNumberItem : MonoBehaviour
{
    public TMP_Text NameText;
    public TMP_Text PhoneNumTex;
    public Button DeleteButton;

    private void Start()
    {
        DeleteButton.onClick.AddListener(OnDeleteButtonClick);
    }


    private void OnDeleteButtonClick()
    {
        PlayerPrefs.DeleteKey(NameText.text);

        string str = PlayerPrefs.GetString("PhoneName", "");
        int index = str.IndexOf(NameText.text);
        str = str.Remove(index - 1, NameText.text.Length + 1);

        PlayerPrefs.SetString("PhoneName", str);

        Destroy(gameObject);
    }
    public void SetInfo(string name, string phone)
    {
        NameText.text = name;
        PhoneNumTex.text = phone;
    }
}
