using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Data : MonoBehaviour
{
   public Button Close;
   public GameObject close;
   public GameObject main_page;


    public Toggle EstimateFloodsToggle;
    public Toggle SearchFloodsToggle;


    private void OnEnable()
    {
        EstimateFloodsToggle.isOn = false;
        SearchFloodsToggle.isOn = true;
    }
    
    void Start()
    {
        Close.onClick.AddListener(CloseEvent);
    }

    void CloseEvent()
    {
        close.SetActive(false);
        main_page.SetActive(true);
    }

    private void OnDisable()
    {
        EstimateFloodsToggle.isOn = false;
        SearchFloodsToggle.isOn = true;
    }
}