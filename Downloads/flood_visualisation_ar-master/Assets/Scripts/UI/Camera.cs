using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;

public class Camera : MonoBehaviour
{
    public GameObject tips;
    public TMP_Text text;
    public Toggle toggle;
    public Button camera;
    public Button map;
    public Button recenter;
    public Button ScanQR;
    public GameObject close;
    public GameObject main_page;
    public GameObject Map;

    public QRCodeScanner qrScanner;
    public TMPro.TMP_Text qrScannerText;

    public FloodDataManager floodDataManager;
    


    private void OnEnable()
    {
        toggle.isOn = false;
        //text.gameObject.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        camera.onClick.AddListener(CloseEvent);
        map.onClick.AddListener(OpenMap);

        recenter.onClick.AddListener(RecenterHeight);

        if(ScanQR && qrScanner)
        {
            ScanQR.onClick.AddListener(ScanQREnable);
        } else
        {
            Debug.LogWarning("ScanQR and QRScanner not defined on Camera.cs. This is normal for UIScene");
        }

        toggle.onValueChanged.AddListener(OnToggleValueChanged);


        floodDataManager.SetInfoPanels(true);

    }

    private void OnToggleValueChanged(bool isOn)
    {

        //tips.SetActive(!isOn);
        floodDataManager.SetInfoPanels(!isOn);
    }

    void CloseEvent()
    {
        if (close != null)
        {
            close.SetActive(false);
        }

        if (main_page != null)
        {
            main_page.SetActive(true);
        }

        SceneManager.LoadScene("UIScene", LoadSceneMode.Single);
    }

    void OpenMap()
    {
        Map.SetActive(true);
        close.SetActive(false);
    }

    void RecenterHeight()
    {
        HeightVisualiser[] visualisers = GameObject.FindObjectsOfType<HeightVisualiser>();

        foreach(HeightVisualiser vis in visualisers)
        {
            vis.ForceRecenter();
        }
    }

    void ScanQREnable()
    {
        if(qrScannerText)
        {
            qrScannerText.text = "Scan QR Code";
        }
        qrScanner.EnableSearch();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
