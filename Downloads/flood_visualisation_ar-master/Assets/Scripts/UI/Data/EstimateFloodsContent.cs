using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EstimateFloodsContent : MonoBehaviour
{
    public Transform ItemParent;
    public TMP_InputField SearchInput;
    private List<string> KeyWords = new List<string>();

    private void OnEnable()
    {
        SearchInput.text = "";

        for (int i = 0; i < ItemParent.childCount; i++)
        {
            ItemParent.GetChild(i).gameObject.SetActive(true);
            KeyWords.Add(ItemParent.GetChild(i).GetComponent<EstimateFloodsItem>().Key.ToLower());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SearchInput.onValueChanged.AddListener(OnSearchInputValueChanged);
    }

    private void OnSearchInputValueChanged(string value)
    {
        for (int i = 0; i < ItemParent.childCount; i++)
        {
            ItemParent.GetChild(i).gameObject.SetActive(false);
        }
        for (int i = 0; i < KeyWords.Count; i++)
        {
            if (KeyWords[i].Contains(value.ToLower()))
            {
                ItemParent.GetChild(i).gameObject.SetActive(true);
            }
        }
    }


    private void OnDisable()
    {
        SearchInput.text = "";
        KeyWords.Clear();
        for (int i = 0; i < ItemParent.childCount; i++)
        {
            ItemParent.GetChild(i).gameObject.SetActive(true);
        }
    }

}
