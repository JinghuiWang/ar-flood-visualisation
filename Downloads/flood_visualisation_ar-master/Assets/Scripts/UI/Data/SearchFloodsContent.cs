using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SearchFloodsContent : MonoBehaviour
{

    public TMP_Dropdown TyepDropdown;
    public TMP_InputField SearchInput;


    public List<GameObject> ScrollViewList;


    public Transform FloodItemParent;
    public Transform RainItemParent;


    private List<string> FloodKeyList = new List<string>();
    private List<string> RainKeyList = new List<string>();


    private void OnEnable()
    {

        TyepDropdown.value = 0;
        OnDropdownValueChanged(0);


        for (int i = 0; i < FloodItemParent.childCount; i++)
        {
            FloodKeyList.Add(FloodItemParent.GetChild(i).GetComponent<SearchFloodItem>().Key.ToLower());
        }


        for (int i = 0; i < RainItemParent.childCount; i++)
        {
            RainKeyList.Add(RainItemParent.GetChild(i).GetComponent<SearchRainItem>().Key.ToLower());
        }
    }

    private void Start()
    {
        TyepDropdown.onValueChanged.AddListener(OnDropdownValueChanged);
        SearchInput.onValueChanged.AddListener(OnSearchInputValueChanged);
    }


    private void OnDisable()
    {
        SearchInput.text = "";
        FloodKeyList.Clear();
        RainKeyList.Clear();
    }

    private void OnDropdownValueChanged(int value)
    {
        for (int i = 0; i < ScrollViewList.Count; i++)
        {
            ScrollViewList[i].SetActive(i == value);
        }
    }


    private void OnSearchInputValueChanged(string value)
    {
        //Flood
        if (TyepDropdown.value == 0)
        {
            for (int i = 0; i < FloodItemParent.childCount; i++)
            {
                FloodItemParent.GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < FloodKeyList.Count; i++)
            {
                if (FloodKeyList[i].Contains(value.ToLower()))
                {
                    FloodItemParent.GetChild(i).gameObject.SetActive(true);
                }
            }
        }
        //Rain
        else if (TyepDropdown.value == 1)
        {
            for (int i = 0; i < RainItemParent.childCount; i++)
            {
                RainItemParent.GetChild(i).gameObject.SetActive(false);
            }
            for (int i = 0; i < RainKeyList.Count; i++)
            {
                if (RainKeyList[i].Contains(value.ToLower()))
                {
                    RainItemParent.GetChild(i).gameObject.SetActive(true);
                }
            }
        }
    }
}
