using Google.Maps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARMapsServiceMover : MonoBehaviour
{
    ARTrackedImageManager instanceManager;
    public ARSession arSession;

    public ARTrackedImage googleMapsImage;

    MapsService mapService;

    // Start is called before the first frame update
    void Start()
    {
        instanceManager = GetComponent<ARTrackedImageManager>();
        mapService = FindObjectOfType<MapsService>();

        instanceManager.trackedImagesChanged += InstanceManager_trackedImagesChanged;
        ConsoleText.Get()?.AddLine("Started Debug ARSession");

    }

    private void InstanceManager_trackedImagesChanged(ARTrackedImagesChangedEventArgs obj)
    {
        foreach(ARTrackedImage img in obj.added)
        {
            ConsoleText.Get()?.AddLine(string.Format("{0} was added", img.referenceImage.name));
            if(img.referenceImage.name == "ar_marker")
            {
                googleMapsImage = img;

                Vector3 pos = img.transform.position;
                pos.y += 0.01f;
                mapService.transform.position = pos;
                mapService.transform.rotation = img.transform.rotation;
            }
        }

        foreach (ARTrackedImage img in obj.removed)
        {
            ConsoleText.Get()?.AddLine(string.Format("{0} was removed", img.referenceImage.name));
        }

        foreach(ARTrackedImage img in obj.updated)
        {
            if (img.trackableId == googleMapsImage.trackableId && img.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
            {
                Vector3 pos = img.transform.position;
                pos.y += 0.01f;
                mapService.transform.position = pos;
                mapService.transform.rotation = img.transform.rotation;
                //ConsoleText.Get()?.AddLine("Tracking position updated for MapsService");
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        ConsoleText.Get()?.SetFps(string.Format("Frame Time: {0:0.00}ms", Time.smoothDeltaTime));
    }
}
