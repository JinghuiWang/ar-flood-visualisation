using FloodData;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/**
 *  Places a cylinder on the ground with a height and width.
 *  Keeps it locked to the ground plane.
 */
public class HeightVisualiser : MonoBehaviour
{
    public IFloodData FloodSource
    {
        get
        {
            return floodSource;
        }

        set
        {
            floodSource = value;
            // Fill in values
            ShadeColor = floodSource.Color;
            LineColor = floodSource.LineColor;
            height = 1f; // Height is unknown and needs to be updated framebyframe
        }
    }

    private IFloodData floodSource;

    public GameObject arCamera;

    public InfoPanel infoPanel;

    public Color ShadeColor { get { return ShadeColor; } set
        {
            GetComponent<MeshRenderer>().material.color = value;
            shadeColour = value;
        }
    }

    public Color LineColor { get { return lineColour;  } set
        {
            GetComponent<LineRenderer>().startColor = value;
            GetComponent<LineRenderer>().endColor = value;

            lineColour = value;
        } 
    }

    Color shadeColour;
    Color lineColour;

    private CameraHeight arCameraHeight;
    private LineRenderer lineRenderer;

    // Cylinder Mesh (height / 2)
    // HollowCylinder Mesh height 1=1
    // Height is updated by FloodDataManager
    public float height = 1;
    public float width = 1;
    public float heightMultiply = 1;

    public float groundOffset = 0;

    public float RotationAngle = 0f;

    public int offsetLinesOne = 0;

    private float GetOffsetHeight()
    {
        // Height & offset is updated by FloodDataManager
        return height * heightMultiply - groundOffset;
    }

    public int numberOfEdgeLines = 40;

    // Start is called before the first frame update
    void Start()
    {
        if(arCamera == null)
        {
            arCamera = GameObject.Find("AR Camera");
        }
        arCameraHeight = arCamera.GetComponent<CameraHeight>();
        lineRenderer = GetComponent<LineRenderer>();
        UpdateHeight();
        UpdateWidth();
        //UpdateLineRenderer();

        
        
    }

    // Update is called once per frame
    void Update()
    {
        // Width and Height order do not matter
        UpdateWidth();
        UpdateHeight();
        // Must be done after Height
        UpdatePosition();
        // Must be done after Position, Width and Height
        UpdateLineRenderer();

        UpdateInfoPanel();
    }

    void UpdateLineRenderer()
    {
        List<Vector3> lines = new List<Vector3>();
        float degreesPerLine = 360f / (float)numberOfEdgeLines;
        //Debug.Log(string.Format("Degrees per line {0}", degreesPerLine));
        for(int i = 0; i < numberOfEdgeLines; i+=2)
        {
            float radsPerLine = Mathf.Deg2Rad * degreesPerLine;
            float currentAng = radsPerLine * i + (offsetLinesOne * (radsPerLine / 2f));
            float x = (width / 2) * Mathf.Cos(currentAng) + this.transform.position.x;
            float z = (width / 2) * Mathf.Sin(currentAng) + this.transform.position.z;
            float xN = (width / 2) * Mathf.Cos(currentAng + radsPerLine) + this.transform.position.x;
            float zN = (width / 2) * Mathf.Sin(currentAng + radsPerLine) + this.transform.position.z;

            // bottom -> top -> right -> bottom

            lines.Add(new Vector3(x, this.transform.position.y - (GetOffsetHeight() / 2), z));
            lines.Add(new Vector3(x, this.transform.position.y + (GetOffsetHeight() / 2), z));

            lines.Add(new Vector3(xN, this.transform.position.y + (GetOffsetHeight() / 2), zN));
            lines.Add(new Vector3(xN, this.transform.position.y - (GetOffsetHeight() / 2), zN));


        }
        lineRenderer.positionCount = lines.Count;
        lineRenderer.SetPositions(lines.ToArray());
    }

    void UpdateHeight()
    {
        Vector3 scale = this.transform.localScale;
        scale.y = GetOffsetHeight();
        this.transform.localScale = scale;
    }

    void UpdateWidth()
    {
        Vector3 scale = this.transform.localScale;
        scale.x = width;
        scale.z = width;
        this.transform.localScale = scale;
    }

    void UpdatePosition(bool forceCentre = false)
    {
        // Distance Snapping
        Vector3 currentPosition = this.transform.position;
        Vector2 pos2d = new Vector2(currentPosition.x, currentPosition.z);
        Vector2 cameraPos2d = new Vector2(arCamera.transform.position.x, arCamera.transform.position.z);

        float horizontalDistance = Vector2.Distance(pos2d, cameraPos2d);

        // If we are more than 3m away (2d not 3d space), teleport the object to us :)
        if(horizontalDistance > 4.5 || forceCentre)
        {
            currentPosition.x = arCamera.transform.position.x;
            currentPosition.z = arCamera.transform.position.z;
        }

        currentPosition.y = arCamera.transform.position.y;
        if (arCameraHeight.GeographicHeightReady)
        {
            currentPosition.y -= arCameraHeight.GeographicHeight; // Geographic height is good :)
        } else
        {
            // TODO: handle no geographic height! We probably need to show a warning or some visual display.
            currentPosition.y -= arCameraHeight.Height;
        }
        // Coordiates relative to center - add half object height + offset
        currentPosition.y += (GetOffsetHeight() / 2) + groundOffset; // This will keep it anchored to ground
        this.transform.position = currentPosition;
    }

    public void ForceRecenter()
    {
        UpdatePosition(true);
    }

    void UpdateInfoPanel()
    {
        if (floodSource != null && infoPanel != null)
        {
            infoPanel.SetTitle(floodSource.Name);
            infoPanel.SetHeight(height);

            Vector3 position = this.transform.position;
            float angle = RotationAngle;
            position.x += Mathf.Sin(Mathf.Deg2Rad * angle) * width * 0.48f;
            position.z += Mathf.Cos(Mathf.Deg2Rad * angle) * width * 0.48f;
            position.y += (GetOffsetHeight() / 2f) - 0.01f;
            infoPanel.transform.position = position;

            infoPanel.transform.rotation = Quaternion.Euler(0f, angle, 0f);
        }
    }
}
