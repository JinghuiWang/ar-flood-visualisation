using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugText : MonoBehaviour
{
    public static DebugText Get()
    {
        return FindObjectOfType<DebugText>();
    }

    public int maxLines = 50;
    TextMeshProUGUI consoleDebug;
    string[] consoleLines;
    int currentIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        consoleDebug = GetComponent<TextMeshProUGUI>();
        consoleLines = new string[maxLines];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddLine(string line)
    {
        consoleLines[currentIndex] = line;
        currentIndex++;
        currentIndex %= consoleLines.Length;

        string text = "";

        for(int i = 0; i < consoleLines.Length; i++)
        {
            int currentPos = (i + currentIndex) % consoleLines.Length;
            text += consoleLines[currentPos] + "\n";
        }

        consoleDebug.text = text;
    }
}
