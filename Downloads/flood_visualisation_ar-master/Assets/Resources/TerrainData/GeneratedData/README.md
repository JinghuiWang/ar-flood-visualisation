# Data Source
This data was generated from the Queensland LiDAR Data - SE Queensland 2014 Project


# Data Format
The .bin files are storing an array of 1000x1000 floating point values (float32)

There is a lookup.json that contains the coordinates (in EPSG:4326 format) of each square area the data represents. 

When iterating through the lookup data, make sure to find the top value first, then find the left value as needed <br/>*(the JSON data is ordered top first, and this will skip unnecessary comparisons)*

This file can then be loaded, and in 1m incrememnts (or divided between the coordinates) the floats can be indexed.<br>
`array[x*1000 + y]`