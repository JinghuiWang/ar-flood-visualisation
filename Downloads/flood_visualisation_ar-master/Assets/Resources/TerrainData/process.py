import json
import struct
import rasterio
import rasterio.features
import rasterio.warp
from os import listdir
from os.path import isfile, join

path = './QLD Government/DEM/1 Metre'
outPath = './GeneratedData'

fileList = [file for file in listdir(path) if isfile(join(path, file))]

lookupData = []

for file in fileList:
    if not file.endswith('.tif'):
        continue
    
    print(f"Processing {file}")
    with rasterio.open(join(path, file)) as dataset:
        mask = dataset.dataset_mask()

        dataOut = {}
        dataOut["data"] = []
        dataOut["lookup"] = {}

        for geom, val in rasterio.features.shapes(mask, transform=dataset.transform):
            print(dataset.crs)
            #geom = rasterio.warp.transform_geom(
            #    dataset.crs, 'EPSG:4326', geom, precision=10)
            # {"type": "Polygon", "coordinates": [
            # [  EW           NW
            #   [152.979762, -27.474556], Top left
            #   [152.979761, -27.483584], bottom left
            #   [152.989883, -27.483585], bottom right
            #   [152.989884, -27.474557], top right
            #   [152.979762, -27.474556]  Top left
            # ]]}
            left = geom["coordinates"][0][0][0]
            top = geom["coordinates"][0][2][1]
            right = geom["coordinates"][0][2][0]
            bottom = geom["coordinates"][0][0][1]
            print(geom)

            if top <= bottom:
                print("Top <= bottom")
            if left >= right:
                print("Left >= Right")
            
            dataOut["lookup"] = {"top": top, "bottom": bottom, "left": left, "right": right}

        band1 = dataset.read(1)
        for y in band1:
            for x in y:
                dataOut["data"].append(float(x))

        dataOut["lookup"]["file"] = file.replace(".tif", "")
        lookupData.append(dataOut["lookup"])

        with open(join(outPath, file.replace('.tif', '.bytes')), "wb") as outFile:
            outStruct = struct.pack("f" * len(dataOut["data"]), *dataOut["data"]);
            outFile.write(outStruct)

        
with open(join(outPath, "lookup.json"), "w") as lookupFile:
    lookupFile.write(json.dumps({"data": lookupData}))
        