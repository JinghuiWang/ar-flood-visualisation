# Installing Unity
Install Unity from the official Unity website.

Download Unity 2022.1.14f1 and enable Android Build Support and/or iOS Build Support.

# Building and Compiling
This project is a Unity project, and will require Unity 2022.1.14f1 or higher compatible versions.

To build this project, load the project into Unity, and open the Build Settings... menu.
Make sure that the project is set to Android or iOS support, and you have the necessary unity prerequisites.

These prerequisites can be installed via the Unity Hub when installing Unity 2022.1.14f1

Once the project is built, the application can be loaded on to a phone by transferring the file
or using XCode to properly load it onto an iOS device.

The application can also be loaded on to a phone using Patch and Run in the Android Build Settings.

# Installing the Application (Android)
The current up-to-date build of the application is too large to fit in our online GitLab.<br>
It can be found for download below at either of the below sites:<br>
https://jagdadd.au/build_22_10_17_2307.apk<br>
https://mega.nz/file/arR0XK6J#rOQ0d-i2fgjDgY-EZ2rkZvqxfFH9mIwnDqdlZP3jdn0

Copy this file on to a mobile device, and install it by executing the .apk file.

You likely will need to enable untrusted sources to install the application.

# Our Scripts
Below is a list of folders that are directly our work:
- Assets/Scripts/*
- Assets/Scenes/*
- Assets/Resources/*
- Data/FloodStudyOverlandFlow/*
- ExpressServer/*

# References and Sources
## Software
**Unity Engine 2022.1.14f1:** https://unity.com/<br>
**ZXing .NET**: https://github.com/micjahn/ZXing.Net<br>
**Google Maps for Unity**: https://developers.google.com/maps/documentation/gaming/overview_musk

## Data
**Terrain Height Data:** https://elevation.fsdf.org.au/<br>
*Data was converted to an alternate format locally*

**Predicted Flooding:** https://www.data.brisbane.qld.gov.au/data/dataset/flood-study-citywide-overland-flow-inner-west-sub-model<br>
All flooding data was taken from the Flood Study - Citywide Overland Flow datasets, available on the
Brisbane council website.

<br>
*This data was locally converted to a smaller format for mobile devices*